package com.mycompany.swing1;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;

class MyActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener : Action");
    }

}

public class HelloMe implements ActionListener {

    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(400, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JLabel lblYourName = new JLabel("Your name : ");
        lblYourName.setSize(75, 30);
        lblYourName.setLocation(5, 5);
        lblYourName.setBackground(Color.WHITE);
        lblYourName.setOpaque(true);

        JTextField txtYourName = new JTextField();
        txtYourName.setSize(200, 30);
        txtYourName.setLocation(90, 5);

        JButton btnHello = new JButton("Hello");
        btnHello.setSize(80, 20);
        btnHello.setLocation(90, 40);

        MyActionListener myActionListener = new MyActionListener();
        btnHello.addActionListener(myActionListener);
        btnHello.addActionListener(new HelloMe());

        ActionListener actionListener = new ActionListener(){
        	@Override
    		public void actionPerformed(ActionEvent e) {
        		System.out.println("Anonymous Class : Action");
        	}
        };
        btnHello.addActionListener(actionListener);

        

        JLabel lblHello = new JLabel("Hello..", JLabel.CENTER);
        lblHello.setSize(200, 20);
        lblHello.setLocation(90, 70);
        lblHello.setBackground(Color.WHITE);
        lblHello.setOpaque(true);

        frmMain.setLayout(null);
        frmMain.add(lblYourName);
        frmMain.add(txtYourName);
        frmMain.add(btnHello);
        frmMain.add(lblHello);

        btnHello.addActionListener(new ActionListener(){
        	@Override
    		public void actionPerformed(ActionEvent e) {
        		String name = txtYourName.getText();
        		lblHello.setText("Hello " + name);
        	}
        });

        frmMain.setVisible(true);
    }
        @Override
    	public void actionPerformed(ActionEvent e) {
        	System.out.println("HelloMe : Action");
        }

}
